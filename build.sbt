name := "time-series"

version := "1.0"

scalaVersion := "2.11.8"

scalacOptions in Test ++= Seq("-Yrangepos")

lazy val akkaVersion = "2.4.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.specs2" %% "specs2-core" % "3.8.4" % "test"
)


