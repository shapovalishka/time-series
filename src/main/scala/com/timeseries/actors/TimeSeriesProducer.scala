package com.timeseries.actors

import akka.actor.{Actor, PoisonPill, Props}
import akka.actor.Actor.Receive
import akka.event.Logging
import com.timeseries.service.TimeSeriesService.TimeSeries
import com.timeseries.service.TimeSeriesSourceImpl

object TimeSeriesProducer {

  case object Start

  case object Finish

  case object GiveTimeSeries

}

class TimeSeriesProducer(inputFilePath: String, windowSize: Long)  extends Actor {

  import TimeSeriesProducer._

  val log = Logging(context.system, this)
  val printer = context.actorOf(Props(classOf[MovingAveragePrinter]), "moving-average-printer")
  val worker = context.actorOf(Props(classOf[MovingAverage], windowSize, printer), "moving-average")

  override def receive: Receive = {
    case Start =>
      printer ! MovingAveragePrinter.Init
      val stream = TimeSeriesSourceImpl(inputFilePath).getStream
      context.become(readingStream(stream.iterator))
      self ! GiveTimeSeries
    case Finish =>
      // TODO: shutdown hook
  }

  def readingStream(iterator: Iterator[Option[TimeSeries]]): Receive = {
    case GiveTimeSeries =>
        iterator.next() match {
          case Some(ts) =>
            context.become(readingStream(iterator))
            worker ! ts
          case None =>
            context.become(receive)
            worker ! MovingAverage.Clean
            self ! Finish
        }
  }
}



