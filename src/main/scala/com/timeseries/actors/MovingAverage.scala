package com.timeseries.actors

import akka.actor.{Actor, ActorRef}
import akka.actor.Actor.Receive
import com.timeseries.service.TimeSeriesService.TimeSeries

object MovingAverage {

  case object Clean

  case class Average(ts: TimeSeries, counter: Long, summ: Double, max: Double, min: Double)
}

class MovingAverage(windowSize: Long, printer: ActorRef) extends Actor {

  import MovingAverage._

  override def receive: Receive = {
    case ts : TimeSeries =>
      val av = Average(ts, 1L, ts.value, ts.value, ts.value)
      printer ! MovingAveragePrinter.Print(av)
      context.become(movingWindow(List(av), av))
      sender() ! TimeSeriesProducer.GiveTimeSeries
  }

  def movingWindow(window: List[Average], last: Average): Receive = {
    case Clean => context.become(receive)
    case ts: TimeSeries =>
      val (out, in) = window.partition(av =>
        av.ts.seconds < ts.seconds - windowSize)
      val inner = in.map(_.ts) ++ Seq(ts)
      val av = Average(ts = ts,
        counter = last.counter + 1 - out.size,
        summ = last.summ + ts.value - out.map(_.ts.value).sum,
        max = inner.map(_.value).max,
        min = inner.map(_.value).min)
      printer ! MovingAveragePrinter.Print(av)
      context.become(movingWindow(in ++ Seq(av), av))
      sender() ! TimeSeriesProducer.GiveTimeSeries

  }
}

object MovingAveragePrinter {
  case object Init

  case class Print(av: MovingAverage.Average)
}

class MovingAveragePrinter extends Actor {

  import MovingAverage._
  import MovingAveragePrinter._

  override def receive: Receive = {
    case Init =>
      println(
        """
          |T          V       N RS      MinV    MaxV
          |---------------------------------------------
        """.stripMargin)
    case Print(av) =>
      println(f"${av.ts.seconds} ${av.ts.value}%2.5f ${av.counter} ${av.summ}%2.5f ${av.min}%2.5f ${av.max}%2.5f")
  }
}