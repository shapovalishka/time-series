package com.timeseries

import java.io.{File, FileInputStream}

import akka.actor.{ActorSystem, Props}
import com.timeseries.actors.TimeSeriesProducer

import scala.util.Try

object Main extends App {
  val path = Try(args(0)).toOption.filter(new File(_).exists()).getOrElse {
    println(
      """
        |Failed to read input file path from command-line arguments or such file doesn't exist.
        |Please run:
        |
        |sbt "run-main com.timeseries.Main /path/to/the/file"
        |
        |Here is an example result:
      """.stripMargin)
    getClass
      .getClassLoader
      .getResource("example-data")
      .getPath
  }
  val windowSize = 60L
  val system = ActorSystem("TimeSeriesSystem")
  val myActor = system.actorOf(Props(classOf[TimeSeriesProducer], path, windowSize), name = "time-series-producer")
  myActor ! TimeSeriesProducer.Start
}


