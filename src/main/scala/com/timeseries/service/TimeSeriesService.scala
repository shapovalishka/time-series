package com.timeseries.service

import java.io.{BufferedReader, FileInputStream, IOException, InputStreamReader}

import com.timeseries.service.TimeSeriesService.TimeSeries

import scala.util.Try

object TimeSeriesService {

  case class TimeSeries(seconds: Long, value: Double)

}

trait TimeSeriesSource {

  def getStream: Stream[Option[TimeSeries]]

}

case class TimeSeriesSourceImpl(val path: String) extends TimeSeriesSource {

  import TimeSeriesService._

  override def getStream: Stream[Option[TimeSeries]] = {
    val fIn = new FileInputStream(path)
    val in = new BufferedReader(new InputStreamReader(fIn))
    Stream.continually(Try(in readLine)).map(lineTry => lineTry.map { line =>
      val splitResult = line.split("(\\t+)|(\\s+)")
      Option(TimeSeries(
        seconds = splitResult.head.toLong,
        value = splitResult.tail.head.toDouble
      ))
    } recover {
      case _ =>
        None
    } get)
  }
}
